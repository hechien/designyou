class AddAssetsCountToUsers < ActiveRecord::Migration
  def change
    add_column :users, :assets_count, :integer, default: 0
  end
end
