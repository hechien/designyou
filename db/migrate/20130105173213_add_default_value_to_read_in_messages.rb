class AddDefaultValueToReadInMessages < ActiveRecord::Migration
  def up
    change_column :messages, :read, :boolean, default: false
  end

  def down
    change_column :messages, :read, :boolean
  end
end
