class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.belongs_to :user
      t.string :name
      t.string :content, limit: 500
      t.boolean :read

      t.timestamps
    end
    add_index :messages, :user_id
  end
end
