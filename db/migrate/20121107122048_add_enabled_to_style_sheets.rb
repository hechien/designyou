class AddEnabledToStyleSheets < ActiveRecord::Migration
  def change
    add_column :style_sheets, :enabled, :boolean, default: false
  end
end
