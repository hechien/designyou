class CreateStyleSheets < ActiveRecord::Migration
  def change
    create_table :style_sheets do |t|
      t.belongs_to :user
      t.text :content

      t.timestamps
    end
    add_index :style_sheets, :user_id
  end
end
