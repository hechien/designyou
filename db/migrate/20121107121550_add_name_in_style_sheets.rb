class AddNameInStyleSheets < ActiveRecord::Migration
  def up
    add_column :style_sheets, :name, :string
  end

  def down
    remove_column :style_sheets, :name
  end
end
