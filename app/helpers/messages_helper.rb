module MessagesHelper

  def read_icon(is_read)
    klass = is_read ? "icon-ok" : "icon-remove"
    raw content_tag :i, nil, { class: klass }
  end
end
