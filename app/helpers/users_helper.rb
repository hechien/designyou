module UsersHelper
  def authorized_area(user)
    yield if user_signed_in? && user === current_user
  end
end
