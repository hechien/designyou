class UsersController < ApplicationController
  before_filter :authenticate_user!, only: [:edit, :update]

  def index
    @page_title = "List users"
    @users = User.all
    @assets = Asset.random(24)
  end

  def show
    @user = User.includes(:assets).find(params[:id].to_i)
    @stylesheet_file = @user.stylesheets.where({enabled: true}).first || nil
  end

  def edit
    @user = current_user
    @style_sheets = @user.stylesheets || []
    @messages = @user.messages.unread.recent.limit(6)
  end

  def update
    @user = current_user
    @style_sheets = @user.stylesheets || []
    @messages = @user.messages.unread.recent.limit(6)

    asset = params[:user].delete(:asset)
    @user.assets.build({asset: asset}) if asset

    if @user.update_attributes(params[:user])
      redirect_to @user
    else
      render :edit
    end
  end
end
