class AssetsController < ApplicationController

  before_filter :authenticate_user!

  def drag_upload
    assets = params[:assets]
    errors = []
    assets.each do |asset|
      result = current_user.assets.create(asset)
      errors << result.errors.full_messages unless result.errors.blank?
    end

    json = {}
    json[:errors] = errors unless errors.blank?

    render json: json
  end

  def destroy

    current_user.assets.find(params[:id].to_i).destroy

    respond_to do |format|
      format.html{
        if request.xhr?
          render js: "remove_image(#{params[:id]})"
        else
          redirect_to user_path(current_user)
        end
      }
    end
  end
end