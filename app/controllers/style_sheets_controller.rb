class StyleSheetsController < ApplicationController
  before_filter :authenticate_user!
  # GET /style_sheets
  # GET /style_sheets.json
  def index
    @style_sheets = current_user.stylesheets.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @style_sheets }
    end
  end

  # GET /style_sheets/1
  # GET /style_sheets/1.json
  def show
    @style_sheet = current_user.stylesheets.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.css {
        render text: @style_sheet.content
      }
      format.json { render json: @style_sheet }
    end
  end

  # GET /style_sheets/new
  # GET /style_sheets/new.json
  def new
    @style_sheet = current_user.stylesheets.build

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @style_sheet }
    end
  end

  # GET /style_sheets/1/edit
  def edit
    @style_sheet = current_user.stylesheets.find(params[:id])
  end

  # POST /style_sheets
  # POST /style_sheets.json
  def create
    @style_sheet = current_user.stylesheets.build(params[:style_sheet])

    respond_to do |format|
      if @style_sheet.save
        format.html { redirect_to edit_user_path(current_user), notice: 'Style sheet was successfully created.' }
        format.json { render json: @style_sheet, status: :created, location: @style_sheet }
      else
        format.html { render action: "new" }
        format.json { render json: @style_sheet.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /style_sheets/1
  # PUT /style_sheets/1.json
  def update
    @style_sheet = current_user.stylesheets.find(params[:id])

    respond_to do |format|
      if @style_sheet.update_attributes(params[:style_sheet])
        format.html { redirect_to edit_user_path(current_user), notice: 'Style sheet was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @style_sheet.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /style_sheets/1
  # DELETE /style_sheets/1.json
  def destroy
    @style_sheet = current_user.stylesheets.find(params[:id])
    @style_sheet.destroy

    respond_to do |format|
      format.html { redirect_to style_sheets_url }
      format.json { head :no_content }
    end
  end

  def toggle
    css = StyleSheet.find(params[:id])
    current_state = css.enabled
    ActiveRecord::Base.connection.execute("UPDATE `style_sheets` SET `style_sheets`.`enabled` = 0")
    css.update_attribute(:enabled, !current_state)

    redirect_to edit_user_path(current_user)
  end
end
