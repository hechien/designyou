class Message < ActiveRecord::Base
  belongs_to :user
  attr_accessible :content, :name, :read

  scope :unread, where(read: false)
  scope :recent, order('id DESC')

  validates :name, :content, presence: true
end
