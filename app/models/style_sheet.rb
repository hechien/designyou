class StyleSheet < ActiveRecord::Base
  belongs_to :user
  attr_accessible :name, :content
end
