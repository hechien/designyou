class Asset < ActiveRecord::Base
  attr_accessible :asset

  belongs_to :user, counter_cache: true

  scope :recent, order('id DESC')

  def self.random(seed = 12)
    ids = recent.limit(100).pluck(:id)
    ids = ids.shuffle[0...seed]
    Asset.includes(:user).recent.find(ids)
  end

  has_attached_file :asset, styles: { thumb: '300x300>' }, whiny: false
  validates_attachment :asset, :content_type => { :content_type => ["image/gif", "image/jpeg", "image/png", "image/tiff"] }
end
