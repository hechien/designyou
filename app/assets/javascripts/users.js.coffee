# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$ ->
  $('#drag_indicator').hide()

  # Event list
  # - dragstart
  # - dragenter
  # - dragover
  # - dragleave
  # - drag
  # - drop
  # - dragend

  $(document).bind 'drop', (e) ->
    e.preventDefault()
    e.stopPropagation()
    upload(e.originalEvent.dataTransfer.files)
  .on 'dragover', (e) ->
    $('#drag_indicator').show()
    e.preventDefault()
    e.stopPropagation()
  .on 'drop', (e) ->
    $('#drag_indicator').hide()
    e.preventDefault()
    e.stopPropagation()

  $('#drag_indicator').on 'dragleave', (e) ->
    $('#drag_indicator').hide()
    e.preventDefault()
    e.stopPropagation()

  upload = (files) ->
    if typeof(FormData) isnt 'undefined'
      form = new FormData()
      form.append('path', '/')
      for file in files
        form.append('assets[][asset]', file)

      $.ajax {
        url: '/files/drag_upload',
        data: form,
        type: 'POST',
        contentType: false,
        processData: false,
        beforeSend: ->
          $('#uploading-modal').modal('show');
        complete: ->
          $('#uploading-modal').modal('hide');
        error: (data) ->
          $('#uploading-fail-modal').modal('show');
      }

  window.remove_image = (image_id) ->
    $("#asset-#{image_id}").remove()

$('[data-toggle="modal"]').on 'click', (e) ->
  target = $(this).data('target')
  $("##{target}").modal({})

$('#description blockquote').html($('#description blockquote').text().split(/\n/).join("<br />"))